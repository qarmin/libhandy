### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

apt install -y git
git clone https://source.puri.sm/Librem5/libhandy.git
cd libhandy

#Z#
apt -y install build-essential
apt build-dep -y .

meson build 

ninja -C build

```

### QTCreator Includes
```
/usr/include/exiv2
/usr/lib/x86_64-linux-gnu/glib-2.0/include
```
